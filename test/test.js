'use strict';

const compiler = require('../src/compiler');
const parser = require('../src/parser');
const path = require('path');
const fs = require('fs');
const rpcCall = require('kool-makerpccall');
const call = (method, params = []) => rpcCall('http://localhost:8545', method, params);
const emasm = require('emasm');
const makeConstructor = require('emasm/macros/make-constructor');
const { stripHexPrefix, addHexPrefix } = require('ethereumjs-util');
const abi = require('web3-eth-abi');
const encodeFunctionCall = abi.encodeFunctionCall.bind(abi);
describe('contract compilation', async () => {
	const src = fs.readFileSync(path.join(__dirname, 'sample.snuff'), 'utf8');
	const [ from ] = await call('eth_accounts');
	const contract = addHexPrefix(compiler(parser(src), 'RUN_CODE'));
	const { contractAddress } = await call('eth_getTransactionReceipt', [ await call('eth_sendTransaction', [{
		from,
    to: contract,
    data: emasm(makeConstructor(['bytes:contract', [ contract ] ])),
		gas: 6e6,
		gasPrice: 1
	}]) ]);
	console.log(await call('eth_call', [{
		to: contractAddress,
		data: '0x0'
	}]));
})
